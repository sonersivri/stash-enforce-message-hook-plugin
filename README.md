Provides a hook for restricting pushing of commits whose messages don't match a given regex.
